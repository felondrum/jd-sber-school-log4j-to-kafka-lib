package ru.sber.jd;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ObjectMessage;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;



public class LoggerToKafka {
    private Logger log;
    private Class<?> aClass;
    private String author;
    private String project;

    public LoggerToKafka() {}

    public LoggerToKafka(Class<?> aClass, String author, String project) {
        this.aClass = aClass;
        this.author = author;
        this.project = project;
        this.log = LogManager.getLogger(this.aClass);
    }

    public void setaClass(Class<?> aClass) {
        this.aClass = aClass;
        this.log = LogManager.getLogger(this.aClass);
    }

    public void setProject(String project) {
        this.project = project;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void debug(String message) {
        log.debug(getMessage(Level.DEBUG, message));
    }

    public void info(String message) {
        log.info(getMessage(Level.INFO, message));
    }

    public void warn(String message) {
        log.warn(getMessage(Level.WARN, message));
    }

    public void error(String message) {
        log.error(getMessage(Level.ERROR, message));
    }

    public void fatal(String message) {
        log.fatal(getMessage(Level.FATAL, message));
    }

    private ObjectMessage getMessage(Level level, String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        HashMap<Object, Object> map = new HashMap<>();
        map.put("project", project);
        map.put("author", author);
        map.put("text", message);
        map.put("time", dtf.format(now));
        map.put("level", level.getStandardLevel());
        JSONObject jsonObject = new JSONObject(map);
        try {
            Map<String, Object> newMap = new ObjectMapper().readValue(jsonObject.toString(), new TypeReference<Map<String, Object>>() {});
            return new ObjectMessage(newMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }




}
