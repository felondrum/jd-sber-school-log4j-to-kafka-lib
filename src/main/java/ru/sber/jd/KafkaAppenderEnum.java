package ru.sber.jd;

import lombok.Getter;

@Getter
public enum KafkaAppenderEnum {
    KAFKA_LOCAL_STANDARD("localhost:9092", "kafka-log-json", "KafkaLoggerLib");



    private String port;
    private String topic;
    private String name;

    KafkaAppenderEnum(String port, String topic, String name) {
        this.port = port;
        this.topic = topic;
        this.name = name;
    }
}