package ru.sber.jd;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KafkaAppenderBuilder {
    private String port;
    private String topic;
    private String name;
    private LevelRangeFilter levelRangeFilter;

    public KafkaAppenderBuilder() {
        this.port = KafkaAppenderEnum.KAFKA_LOCAL_STANDARD.getPort();
        this.topic = KafkaAppenderEnum.KAFKA_LOCAL_STANDARD.getTopic();
        this.name = KafkaAppenderEnum.KAFKA_LOCAL_STANDARD.getName();
    }
}
