package ru.sber.jd;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoggerComponentCustomBuilder {
    private String name;
    private String additivity;
    private String ref;

    public LoggerComponentCustomBuilder(String name) {
        this.name = name;
        this.additivity = "false";
        this.ref = KafkaAppenderEnum.KAFKA_LOCAL_STANDARD.getName();
    }
}
