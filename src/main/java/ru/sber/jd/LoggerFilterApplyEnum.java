package ru.sber.jd;

import lombok.Getter;

@Getter
public enum LoggerFilterApplyEnum {
    ACCEPT("ACCEPT"),
    DENY("DENY");

    private String name;

    LoggerFilterApplyEnum(String name) {
        this.name = name;
    }
}
