package ru.sber.jd;

import lombok.experimental.Accessors;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

import java.util.List;


@Accessors(chain = true)
public class ConfigLoggerToKafkaService {
    private List<LoggerComponentCustomBuilder> loggersList;
    private List<KafkaAppenderBuilder> kafkaAppenderList;
    private Level rootLevel = Level.INFO;

    public ConfigLoggerToKafkaService() {}

    public void setRootLevel(Level rootLevel) {
        this.rootLevel = rootLevel;
    }

    public void setKafkaAppenderList(List<KafkaAppenderBuilder> kafkaAppenderList) {
        this.kafkaAppenderList = kafkaAppenderList;
    }

    public void setLoggersList(List<LoggerComponentCustomBuilder> loggersList) {
        this.loggersList = loggersList;
    }

    public boolean executeConfigForKafka() {
        ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
        builder.setStatusLevel(Level.DEBUG);
        builder.setConfigurationName("CustomLoggerForKafka");

        if (kafkaAppenderList.isEmpty()) {
            KafkaAppenderBuilder kafkaAppenderBuilder = new KafkaAppenderBuilder();
            kafkaAppenderList.add(kafkaAppenderBuilder);
        }

        for (KafkaAppenderBuilder kafka : kafkaAppenderList) {
            LayoutComponentBuilder layoutComponentBuilder = builder.newLayout("JsonLayout")
                    .addAttribute("complete", "false")
                    .addAttribute("compact", "false")
                    .addAttribute("eventEol", "true")
                    .addAttribute("objectMessageAsJsonObject", "true");

            FilterComponentBuilder filterComponentBuilder = null;
            if (kafka.getLevelRangeFilter() != null) {
                filterComponentBuilder = builder.newFilter("LevelRangeFilter", kafka.getLevelRangeFilter().getOnMatch(), kafka.getLevelRangeFilter().getOnMismatch())
                        .addAttribute("minLevel", kafka.getLevelRangeFilter().getMinLevel())
                        .addAttribute("maxLevel", kafka.getLevelRangeFilter().getMaxLevel());
            }


            PropertyComponentBuilder propertyComponentBuilder = builder.newProperty("bootstrap.servers", kafka.getPort());
            AppenderComponentBuilder appenderComponentBuilder = builder.newAppender(kafka.getName(), "Kafka")
                    .addAttribute("topic", kafka.getTopic())
                    .add(layoutComponentBuilder).addComponent(propertyComponentBuilder);
            if (kafka.getLevelRangeFilter() != null) {appenderComponentBuilder.add(filterComponentBuilder);}
            builder.add(appenderComponentBuilder);
        }


        String defaultConsolePattern = "%style{%d [%t]}{black} %highlight{%-5level: %msg%n%throwable}";
        LayoutComponentBuilder layoutConsole = builder.newLayout("PatternLayout")
                .addAttribute("pattern", defaultConsolePattern);
        AppenderComponentBuilder appenderComponentBuilder1 = builder.newAppender("Console", "Console")
                .addAttribute("target", "SYSTEM_OUT").add(layoutConsole);

        builder.add(appenderComponentBuilder1);

        for (LoggerComponentCustomBuilder logger:loggersList) {
            LoggerComponentBuilder loggerComponentBuilder = builder.newLogger(logger.getName(),"Logger")
                    .addAttribute("additivity", logger.getAdditivity().equals("")?"false":logger.getAdditivity());
            loggerComponentBuilder.add(builder.newAppenderRef(logger.getRef()));
            builder.add(loggerComponentBuilder);
        }

        RootLoggerComponentBuilder rootLogger = builder.newRootLogger(rootLevel);
        rootLogger.add(builder.newAppenderRef("Console"));
        builder.add(rootLogger);
        Configurator.reconfigure(builder.build());
        return true;
    }
}

