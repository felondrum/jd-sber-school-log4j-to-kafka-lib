package ru.sber.jd;

import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.Level;

@Getter
@Setter
public class LevelRangeFilter {
    private String minLevel = Level.DEBUG.name();
    private String maxLevel = Level.ERROR.name();
    private String onMatch = LoggerFilterApplyEnum.ACCEPT.getName();
    private String onMismatch= LoggerFilterApplyEnum.DENY.getName();

    public LevelRangeFilter() {}
}
