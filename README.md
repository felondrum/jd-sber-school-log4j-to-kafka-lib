<h2>Библиотека подключения логирования для передачи по kafka</h2>

Библиотека позволяет быстро подключить и настроить логирование сервиса с передачей логов в сервис kafka.


<h4>Конфигурация для Spring MVC</h4>

<h3> Важно! </h3>

Для корректной работы логирования необходимо отключить в Maven стандартное логирование Spring в вашем приложении!

                     <groupId>org.springframework.boot</groupId>
                            <artifactId>spring-boot-starter-web</artifactId>
                            <exclusions>
                                <exclusion>
                                    <groupId>org.springframework.boot</groupId>
                                    <artifactId>spring-boot-starter-logging</artifactId>
                                </exclusion>
                            </exclusions>


При использовании в приложении Spring Application необходимо создать конфигурацию @Configuration.

Пример базовой конфигурации: {
    
    @Configuration
    public class KafkaLoggerConfiguration {
        @Bean
        public ConfigLoggerToKafkaService executeConfigLog() {
    
            /** (1)*/List<LoggerComponentCustomBuilder> loggerComponentCustomBuilderList = new ArrayList<>();
            /** (2)*/loggerComponentCustomBuilderList.add(new LoggerComponentCustomBuilder("ru.sber.jd"));
            /** (3)*/List<KafkaAppenderBuilder> kafkaAppenderBuilderList = new ArrayList<>();
            /** (4)*/kafkaAppenderBuilderList.add(new KafkaAppenderBuilder());
            /** (5)*/ConfigLoggerToKafkaService configLog = new ConfigLoggerToKafkaService();
            /** (6)*/configLog.setLoggersList(loggerComponentCustomBuilderList);
            /** (7)*/configLog.setKafkaAppenderList(kafkaAppenderBuilderList);
            /** (8)*/configLog.executeConfigForKafka();
            return configLog;
        }
    
        @Bean
        public LoggerToKafka loggerToKafka() {
            return new LoggerToKafka();
        }
    
    
    }
ConfigLoggerToKafkaService :
1) Создание списка логгеров. При конфигурации возможно положить множество разных логгеров.
2) Добавление в список логгера через конструктор. В качестве параметра передается пакет логирования.
3) Создание списка Appender для kafka. При конфигурации возможно создать несколько Appender, например для публикации в разные topics.
4) Добавление в список Appender нового объекта KafkaAppenderBuilder. Поля: port (хост и порт, например localhost:9092) , topic, name
    При создании KafkaAppenderBuilder конструктором по умолчанию поля заполняются базовыми значениями: port= "localhost:9092", topic=kafka-log-json, name=KafkaLoggerLib.
    Для внесения своих параметров можно использовать Setters.
5) Создание объекта конфигурации.
6) Добавление Logger и Appender листов в конфигурацию.
7) Вызов метода конфигурации логирования. 

Пример конфигурации логгера для отдельного класса в конфигурации:
    
    
    @Bean
    public LoggerToKafka loggerToKafkaUserController() {
        LoggerToKafka loggerToKafka = new LoggerToKafka();
        loggerToKafka.setProject("Excel Report Application");
        loggerToKafka.setAuthor("Anton Filippov");
        loggerToKafka.setaClass(UserController.class);
        return loggerToKafka;
    }


LoggerToKafka:
    Добавление логгера библиотеки в конфигурацию для последующей инъекции.
    При расширенной конфигурации для отдельного класса не будет необходимости устанавливать параметры при каждом вызове логгера, при этом это все еще возможно.
        
При запуске приложения в консоль будет выведены параметры конфигурации:
    
    ..
    2020-12-07 14:29:09,054 main DEBUG Building Plugin[name=root, class=org.apache.logging.log4j.core.config.LoggerConfig$RootLogger].
    2020-12-07 14:29:09,055 main DEBUG createLogger(additivity="null", level="INFO", includeLocation="null", ={Console}, ={}, Configuration(CustomLoggerForKafka), Filter=null)
    2020-12-07 14:29:09,056 main DEBUG Building Plugin[name=loggers, class=org.apache.logging.log4j.core.config.LoggersPlugin].
    2020-12-07 14:29:09,056 main DEBUG createLoggers(={ru.sber.jd, root})
    2020-12-07 14:29:09,056 main DEBUG Building Plugin[name=layout, class=org.apache.logging.log4j.core.layout.JsonLayout].    
    ..
        
Верный признак успешной конфигурации.       


<h4>Использование логгера в приложении</h4>
 
 * Произведите инъекцию в ваш класс:
    
 Если вы использовали базовый подход к конфигурации:
    
    @Autowired
    private LoggerToKafka log;
        
 Если вы использовали полную конфигурацию для конкретного класса
 (в параметре аннотации @Qualifier нужно оставить указатель на имя класса конфигурации под класс вызова):
 
    @Resource
    @Qualifier(value = "loggerToKafkaUserController" )
    private LoggerToKafka log;
        
 * Добавьте немного конфигурации в методе.
 Если вы производили конфигурацию для конкретного класса, то эти настройки не являются обязательными, 
 но они все еще возможны:


    ...
    log.setaClass(YOUR_CLASS_NAME.class);
    log.setAuthor("ANY STRING YOU LIKE");
    log.setProject("ANY STRING YOU LIKE");
    ...        
    
 * Логируйте!

    
    ...
    log.debug("ANY_MESSAGE_YOU_WANT_AS_STRING");
    log.info("ANY_MESSAGE_YOU_WANT_AS_STRING"); 
    log.warn("ANY_MESSAGE_YOU_WANT_AS_STRING");
    log.error("ANY_MESSAGE_YOU_WANT_AS_STRING");
    log.fatal("ANY_MESSAGE_YOU_WANT_AS_STRING");  
    ...
      
      
 * Получайте сообщения из kafka из вашей темы в виде json.
Поля с автором, проектом, уровнем, временем и сообщением доступны в виде вложенного json в поле message.


<h3>Приятного использования!</h3>
        